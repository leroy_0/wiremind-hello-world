const express = require('express')
const app = express()

app.get('/hello', (req,res) => {
    res.send("Hello World " + req.query.firstname + " " + req.query.lastname)
})

const port = 8080;
app.listen(port, () => {
    console.log('Server listening on port ' + port)
})